var boardSize
var initSize = 0.045
var dimX = 17
var dimY = 17
var boardArray = create2DArray(dimX, dimY, 0, false)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(13)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  noFill()
  stroke(255)
  fill(0)
  noStroke()
  strokeWeight(1)
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < boardArray.length; i++) {
    for (var j = 0; j < boardArray[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimX * 0.5) + ((dimX + 1) % 2) * 0.5) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimY * 0.5) + ((dimY + 1) % 2) * 0.5) * boardSize * initSize)
      noFill()
      stroke(255)
      strokeWeight(1)
      strokeCap(SQUARE)
      strokeCap(ROUND)
      strokeWeight(boardSize * initSize * 0.25)
      drawTile(boardSize * initSize * 1.0, boardArray[i][j][0], boardArray[i][j][1])
      pop()
    }
  }

  for (var i = 0; i < dimX * 0.5; i++) {
    boardArray[Math.floor(Math.random() * dimX)][Math.floor(Math.random() * dimY)][1] = (boardArray[Math.floor(Math.random() * dimX)][Math.floor(Math.random() * dimY)][1] + 1) % 4
  }
  if (frameCount % 6 === 0) {
    boardArray[Math.floor(Math.random() * dimX)][Math.floor(Math.random() * dimY)] = [Math.floor(Math.random() * 5), Math.floor(Math.random() * 4)]
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}


function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [Math.floor(Math.random() * 5), Math.floor(Math.random() * 4)]
      }
    }
    array[i] = columns
  }
  return array
}

function drawTile(size, type, rotation) {
  if (type === 0) {
    if (rotation % 2 === 0) {
      line(-size * 0.5, 0, size * 0.5, 0)
    } else {
      line(0, -size * 0.5, 0, size * 0.5)
    }
  }
  if (type === 1) {
    if (rotation === 0) {
      line(-size * 0.25, 0, -size * 0.5, 0)
      line(0, -size * 0.25, 0, -size * 0.5)
      line(-size * 0.25, 0, 0, -size * 0.25)
    }
    if (rotation === 1) {
      line(0, -size * 0.25, 0, -size * 0.5)
      line(size * 0.25, 0, size * 0.5, 0)
      line(size * 0.25, 0, 0, -size * 0.25)
    }
    if (rotation === 2) {
      line(size * 0.25, 0, size * 0.5, 0)
      line(0, size * 0.25, 0, size * 0.5)
      line(0, size * 0.25, size * 0.25, 0)
    }
    if (rotation === 3) {
      line(-size * 0.25, 0, -size * 0.5, 0)
      line(0, size * 0.25, 0, size * 0.5)
      line(0, size * 0.25, -size * 0.25, 0)
    }
  }
  if (type === 2) {
    if (rotation % 2 === 0) {
      line(-size * 0.25, 0, -size * 0.5, 0)
      line(0, -size * 0.25, 0, -size * 0.5)
      line(-size * 0.25, 0, 0, -size * 0.25)

      line(size * 0.25, 0, size * 0.5, 0)
      line(0, size * 0.25, 0, size * 0.5)
      line(0, size * 0.25, size * 0.25, 0)
    } else {
      line(0, -size * 0.25, 0, -size * 0.5)
      line(size * 0.25, 0, size * 0.5, 0)
      line(size * 0.25, 0, 0, -size * 0.25)

      line(-size * 0.25, 0, -size * 0.5, 0)
      line(0, size * 0.25, 0, size * 0.5)
      line(0, size * 0.25, -size * 0.25, 0)
    }
  }
  if (type === 3) {
    if (rotation % 2 === 0) {
      line(-size * 0.5, 0, size * 0.5, 0)
      line(0, -size * 0.5, 0, -size * 0.3)
      line(0, size * 0.5, 0, size * 0.3)
    } else {
      line(0, -size * 0.5, 0, size * 0.5)
      line(-size * 0.5, 0, -size * 0.3, 0)
      line(size * 0.5, 0, size * 0.3, 0)
    }
  }
  if (type === 4) {
    if (rotation === 0) {
      line(-size * 0.5, 0, size * 0.5, 0)
      line(0, 0, 0, -size * 0.5)
    }
    if (rotation === 1) {
      line(0, -size * 0.5, 0, size * 0.5)
      line(0, 0, size * 0.5, 0)
    }
    if (rotation === 2) {
      line(-size * 0.5, 0, size * 0.5, 0)
      line(0, 0, 0, size * 0.5)
    }
    if (rotation === 3) {
      line(0, -size * 0.5, 0, size * 0.5)
      line(0, 0, -size * 0.5, 0)
    }
  }
  if (type === 5) {
    line(0, -size * 0.5, 0, size * 0.5)
    line(-size * 0.5, 0, size * 0.5, 0)
  }
}
